<?php

namespace TPSymfony\BibliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hypothesis
 *
 * @ORM\Table(name="hypothesis")
 * @ORM\Entity(repositoryClass="TPSymfony\BibliBundle\Repository\HypothesisRepository")
 */
class Hypothesis
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="hypothesis", type="string", length=255, nullable=true)
     */
    private $hypothesis;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hypothesis
     *
     * @param string $hypothesis
     *
     * @return Hypothesis
     */
    public function setHypothesis($hypothesis)
    {
        $this->hypothesis = $hypothesis;

        return $this;
    }

    /**
     * Get hypothesis
     *
     * @return string
     */
    public function getHypothesis()
    {
        return $this->hypothesis;
    }
    
    /**
     * @return int
     */
    public function getReferencement(){
    	return 10;
    }
}

