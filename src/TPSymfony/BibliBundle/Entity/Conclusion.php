<?php

namespace TPSymfony\BibliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Conclusion
 *
 * @ORM\Table(name="conclusion")
 * @ORM\Entity(repositoryClass="TPSymfony\BibliBundle\Repository\ConclusionRepository")
 */
class Conclusion
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="conclusion", type="string", length=255, nullable=true)
     */
    private $conclusion;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set conclusion
     *
     * @param string $conclusion
     *
     * @return Conclusion
     */
    public function setConclusion($conclusion)
    {
        $this->conclusion = $conclusion;

        return $this;
    }

    /**
     * Get conclusion
     *
     * @return string
     */
    public function getConclusion()
    {
        return $this->conclusion;
    }
    
    /**
     * @return int
     */
    public function getReferencement(){
    	return 20;
    }
}

