<?php

namespace TPSymfony\BibliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contributors
 *
 * @ORM\Table(name="contributors")
 * @ORM\Entity(repositoryClass="TPSymfony\BibliBundle\Repository\ContributorsRepository")
 */
class Contributors
{
	
	/**
	 * @ORM\ManyToOne(targetEntity="TPSymfony\BibliBundle\Entity\Article")
	 * @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
	 */
	private $article;
	
	
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
	 */
	private $name;
	
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="mail", type="string", length=255, nullable=true)
	 */
	private $mail;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255, nullable=true)
	 */
	private $description;
	
	
	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set firstName
	 *
	 * @param string $firstName
	 *
	 * @return Contributors
	 */
	public function setName($name)
	{
		$this->name = $name;
		
		return $this;
	}
	
	/**
	 * Get firstName
	 *
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}
	
	
	
	/**
	 * Set mail
	 *
	 * @param string $mail
	 *
	 * @return Contributors
	 */
	public function setMail($mail)
	{
		$this->mail = $mail;
		
		return $this;
	}
	
	/**
	 * Get mail
	 *
	 * @return string
	 */
	public function getMail()
	{
		return $this->mail;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Contributors
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	/**
	 * @return mixed
	 */
	public function getArticle(){
		return $this->article;
	}
	
	/**
	 * @param mixed $article
	 */
	public function setArticle($article){
		$this->article = $article;
	}

    /**
     * @return int
     */
    public function getReferencement(){
        return 10;
    }
	
}

