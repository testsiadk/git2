<?php

namespace TPSymfony\BibliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Links
 *
 * @ORM\Table(name="links")
 * @ORM\Entity(repositoryClass="TPSymfony\BibliBundle\Repository\LinksRepository")
 */
class Links
{
	/**
	 * @ORM\ManyToOne(targetEntity="TPSymfony\BibliBundle\Entity\Article")
	 *  * @ORM\JoinColumn(name="idArticle", referencedColumnName="idArticle")
	 */
	private $article;
	
	/**
	 * @var int
	 *
	 * @ORM\Column(name="id", type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="link", type="string", length=255, nullable=true)
	 */
	private $link;
	
	
	
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="author", type="string", length=255, nullable=true))
	 */
	private $author;
	
	/**
	 * @var \DateTime
	 *
	 * @ORM\Column(name="date", type="datetime", nullable=true))
	 */
	private $date;
	
	/**
	 * @var string
	 *
	 * @ORM\Column(name="description", type="string", length=255, nullable=true))
	 */
	private $description;
	
	
	/**
	 * Get id
	 *
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * Set link
	 *
	 * @param string $link
	 *
	 * @return Links
	 */
	public function setLink($link)
	{
		$this->link = $link;
		
		return $this;
	}
	
	/**
	 * Get link
	 *
	 * @return string
	 */
	public function getLink()
	{
		return $this->link;
	}
	
	/**
	 * Set author
	 *
	 * @param string $author
	 *
	 * @return Links
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
		
		return $this;
	}
	
	/**
	 * Get author
	 *
	 * @return string
	 */
	public function getAuthor()
	{
		return $this->author;
	}
	
	/**
	 * Set date
	 *
	 * @param \DateTime $date
	 *
	 * @return Links
	 */
	public function setDate($date)
	{
		$this->date = $date;
		
		return $this;
	}
	
	/**
	 * Get date
	 *
	 * @return \DateTime
	 */
	public function getDate()
	{
		return $this->date;
	}
	
	/**
	 * Set description
	 *
	 * @param string $description
	 *
	 * @return Links
	 */
	public function setDescription($description)
	{
		$this->description = $description;
		
		return $this;
	}
	
	/**
	 * Get description
	 *
	 * @return string
	 */
	public function getDescription()
	{
		return $this->description;
	}
	/**
	 * @return mixed
	 */
	public function getArticle(){
		return $this->article;
	}
	
	/**
	 * @param mixed $article
	 */
	public function setArticle($article){
		$this->article = $article;
	}

    /**
     * @return int
     */
    public function getReferencement(){
        return 10;
    }
	
}

