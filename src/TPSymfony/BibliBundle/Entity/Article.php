<?php

namespace TPSymfony\BibliBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use TPSymfony\UserBundle\Entity\User as User;
use TPSymfony\BibliBundle\Entity\Hypothesis as Hypothesis;
use TPSymfony\BibliBundle\Entity\Conclusion as Conclusion;
use TPSymfony\BibliBundle\Entity\Comments as Comments;
use TPSymfony\BibliBundle\Model\AbsArticle;

/**
 * Article
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="TPSymfony\BibliBundle\Repository\ArticleRepository")
 */
class Article extends AbsArticle
{

	/**
	 * @ORM\OneToOne(targetEntity="TPSymfony\BibliBundle\Entity\Hypothesis", cascade={"persist"})
	 */
	private $hypothesis;
	
	/**
	 * @ORM\OneToOne(targetEntity="TPSymfony\BibliBundle\Entity\Conclusion", cascade={"persist"})
	 */
	private $conclusions;
	
	/**
	 * @ORM\OneToOne(targetEntity="TPSymfony\BibliBundle\Entity\Comments", cascade={"persist"})
	 */
	private $comments;

    /**
     * @ORM\ManyToOne(targetEntity="TPSymfony\UserBundle\Entity\User", inversedBy="articles",cascade={"persist"})�
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="idArticle", type="integer", unique=true)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $idArticle;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank(message="Vous devez rentrer le titre de votre article")
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     * @Assert\NotBlank(message="Saississez la date de publication de l'article")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=1024, nullable=true)
     */
    private $tags;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=1024, nullable=true)
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="editor", type="string", length=1024, nullable=true)
     */
    private $editor;
        
    /**
     * @var string
     *
     * @ORM\Column(name="isbn", type="string", length=1024, nullable=true)
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", nullable=true))
     *
     * @Assert\NotBlank(message="Please, upload the product brochure as a PDF file.")
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $brochure;


    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $newBrochure;
    
    /**
     * @ORM\Column(type="string", nullable=true))
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $dataset;
    
    
    /**
     * @ORM\Column(type="string", nullable=true)
     *
     * @Assert\File(mimeTypes={ "application/pdf" })
     */
    private $newDataset;
    
    /**
     * @var int
     *
     * @ORM\Column(name="referencement", type="integer", unique=false)
     */
    protected $referencement;
    
    /**
     * @var string
     *
     * @ORM\Column(name="links", type="string", length=1024, nullable=true)
     */
    private $links;
    
    /**
     * @var string
     *
     * @ORM\Column(name="contributors", type="string", length=1024, nullable=true)
     */
    private $contributors;
    
    /**
     * @var string
     *
     * @ORM\Column(name="experiments", type="string", length=1024, nullable=true)
     */
    private $experiments;
    

/**
     * Set idArticle
     *
     * @param integer $idArticle
     *
     * @return Article
     */
    public function setIdArticle($idArticle)
    {
        $this->idArticle = $idArticle;

        return $this;
    }

    /**
     * Get idArticle
     *
     * @return int
     */
    public function getIdArticle()
    {
        return $this->idArticle;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Article
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Article
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set tags
     *
     * @param string $tags
     *
     * @return Article
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     */
    public function setUser(User $user)
    {
        $this->user = $user;

       // return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set brochure
     *
     * @param string $brochure
     *
     * @return Article
     */
    public function setBrochure($brochure)
    {
        $this->brochure = $brochure;

        return $this;
    }

    /**
     * Get brochure
     *
     * @return string
     */
    public function getBrochure()
    {
        return $this->brochure;
    }


    /**
     * Set newBrochure
     *
     * @param string $newBrochure
     *
     * @return Article
     */
    public function setNewBrochure($newBrochure)
    {
        $this->newBrochure = $newBrochure;

        return $this;
    }

    /**
     * Get newBrochure
     *
     * @return string
     */
    public function getNewBrochure()
    {
        return $this->newBrochure;
    }
    
    /**
     * Set dataset
     *
     * @param string $dataset
     *
     * @return Article
     */
    public function setDataset($dataset)
    {
    	$this->dataset = $dataset;
    	
    	return $this;
    }
    
    /**
     * Get dataset
     *
     * @return string
     */
    public function getDataset()
    {
    	return $this->dataset;
    }
    
    
    /**
     * Set newDataset
     *
     * @param string $newDataset
     *
     * @return Article
     */
    public function setnewDataset($newDataset)
    {
    	$this->newDataset = $newDataset;
    	
    	return $this;
    }
    
    /**
     * Get newDataset
     *
     * @return string
     */
    public function getNewDataset()
    {
    	return $this->newDataset;
    }
	
	/**
     * Set conclusions
     *
     * @param String $conclusions
     *
     * @return Article
     */
    public function setConclusions($conclusions)
    {
		$conclu = new Conclusion();
		$conclu->setConclusion($conclusions);
        $this->conclusions = $conclu;

        return $this;
    }

    /**
     * Get conclusions
     *
     * @return String
     */
    public function getConclusions()
    {
		if (is_null($this->conclusions)) return null;
        return $this->conclusions->getConclusion();
    }
	
		/**
     * Set hypothesis
     *
     * @param String $hypothesis
     *
     * @return Article
     */
    public function setHypothesis($hypothesis)
    {
        $hyp = new Hypothesis();
		$hyp->setHypothesis($hypothesis);
        $this->hypothesis = $hyp;

        return $this;
    }

    /**
     * Get hypothesis
     *
     * @return String
     */
    public function getHypothesis()
    {
		if (is_null($this->hypothesis)) return null;
        return $this->hypothesis->getHypothesis();
    }
	
		/**
     * Set comments
     *
     * @param String $comments
     *
     * @return Article
     */
    public function setComments($comments)
    {
        $comm = new Comments();
		$comm->setComments($comments);
        $this->comments = $comm;

        return $this;
    }

    /**
     * Get comments
     *
     * @return String
     */
    public function getComments()
    {
		if (is_null($this->comments)) return null;
        return $this->comments->getComments();
    }

    /**
     * @return string
     */
    public function getSubtitle(){
        return $this->subtitle;
    }

    /**
     * @return string
     */
    public function getType(){
        return $this->type;
    }

    /**
     * @return string
     */
    public function getEditor(){
        return $this->editor;
    }

    /**
     * @return string
     */
    public function getIsbn(){
        return $this->isbn;
    }

    /**
     * @param string $subtitle
     */
    public function setSubtitle($subtitle){
        $this->subtitle = $subtitle;
    }

    /**
     * @param string $type
     */
    public function setType($type){
        $this->type = $type;
    }

    /**
     * @param string $editor
     */
    public function setEditor($editor){
        $this->editor = $editor;
    }

    /**
     * @param string $isbn
     */
    public function setIsbn($isbn){
        $this->isbn = $isbn;
    }

    /**
     *
     * @return int
     */
    public function getReferencement(){
        return $this->referencement; 
    }

    /**
     * @return string
     */
    public function getLinks(){
    return $this->links;
    }


    /**
     * @return string
     */
    public function getExperiments(){
    return $this->experiments;
    }

    /**
     * @return string
     */
    public function getContributors(){
    return $this->contributors;
    }


    /**
     * @param int $referencement
     */
    public function setReferencement(){
        $this->referencement=0;
        if ($this->hypothesis!=null){
            $this->referencement+=$this->hypothesis->getReferencement();
        }
        if ($this->conclusions!=null){
            $this->referencement+=$this->conclusions->getReferencement();
        }
        if ($this->comments!=null){
            $this->referencement+=$this->comments->getReferencement();
        }
        if ($this->links!=null){
            $this->referencement+=10;
        }
        if ($this->experiments!=null){
            $this->referencement+=10;
        }
        if ($this->contributors!=null){
            $this->referencement+=20;
        }
        if ($this->dataset!=null){
            $this->referencement+=20;
        }
    }

    /**
     * @param string $links
     */
    public function setLinks($links){
    $this->links = $links;
    }

    /**
     * @param string $contributors
     */
    public function setContributors($contributors){
    $this->contributors = $contributors;
    }

    /**
     * @param string $experiments
     */
    public function setExperiments($experiments){
    $this->experiments = $experiments;
    }


}
