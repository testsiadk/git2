<?php


namespace TPSymfony\BibliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class ModifArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
           // ->add('idArticle', 'integer', array('label'=>'Identifiant de l\'article'))
            ->add('title', 'text', array('label'=>'Titre de l\'article'))
            ->add('date', DateType::class, array('widget'=>'choice', 'label' => 'Date de l\'article', 'format' => 'dd MM yyyy'))
            ->add('tags','text',array('label' => 'Tags pour l\'article', 'required' => false))
            ->add('brochure', 'text', array('label' => 'Brochure (PDF file)'))
            ->add('dataset', 'text', array('label' => 'Dataset (PDF file)'))
            ->add('newBrochure', FileType::class, array('label' => 'Modifier le PDF', 'data_class' => null))
            ->add('newDataset', FileType::class, array('label' => 'Modifier les dataset', 'data_class' => null, 'required' => false))
            ->add('comments', TextareaType::class, array('label'=>'Commentaires', 'required' => false))
            ->add('hypothesis', TextareaType::class, array('label'=>'Hypotheses', 'required' => false))
            ->add('conclusions', TextareaType::class, array('label'=>'Conclusions', 'required' => false))
            ->add('experiments', TextareaType::class, array('label'=>'Experiments', 'required' => false))
            ->add('links', TextareaType::class, array('label'=>'Links', 'required' => false))
            ->add('contributors', TextareaType::class, array('label'=>'Contributors', 'required' => false))
            ->add('modifierArticle', SubmitType::class, array('label'=>'Modifier l\'article'))
            ->add('annuler', SubmitType::class, array('label' => 'Annuler'));

    }


    public function getBlockPrefix()
    {
        return 'modifArticleForm';
    }

}
?>







