<?php


namespace TPSymfony\BibliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class RechercheArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('label'=>'Titre', 'required' => false))
            ->add('tags', 'text', array('label'=>'Tags', 'required' => false))
            ->add('date', DateType::class, array('widget'=>'choice', 'label' => 'Date', 'format' => 'dd MM yyyy', 'data' => new \DateTime(), 'required' => false))
            ->add('rechercherArticleByTitle', SubmitType::class, array('label'=>'Rechercher par titre'))
            ->add('rechercherArticleByTags', SubmitType::class, array('label'=>'Rechercher par tags'))
            ->add('rechercherArticleByDate', SubmitType::class, array('label'=>'Rechercher par date'))
        ;
    }

    public function getBlockPrefix()
    {
        return 'rechercheArticleForm';
    }
}
?>



