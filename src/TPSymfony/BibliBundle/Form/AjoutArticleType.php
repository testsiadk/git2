<?php


namespace TPSymfony\BibliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;


class AjoutArticleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('idArticle', 'integer', array('label'=>'Identifiant de l\'article'))
            ->add('title', 'text', array('label'=>'Titre de l\'article *'))
            ->add('subtitle', 'text', array('label'=>'Subtitle *'))
            ->add('type', ChoiceType::class, array(
            		'choices' => array('Book' => 'Book', 'Article' => 'Article', 'InCollection' => 'InCollection ', 'Inproceeding' => 'Inproceeding'),
            		'expanded' => true,
                    'label'=>'Type *'
            ))
            ->add('isbn', 'text', array('label'=>'ISBN *'))
            ->add('editor', 'text', array('label'=>'Editor *'))
            ->add('date', DateType::class, array('widget'=>'choice', 'label' => 'Date de l\'article', 'format' => 'dd MM yyyy', 'data' => new \DateTime()))
            ->add('tags','text',array('label' => 'Tags pour l\'article', 'required' => false))
            ->add('brochure', FileType::class, array('label' => 'Fichier PDF *'))
            ->add('dataset', FileType::class, array('label' => 'Dataset in PDF', 'required' => false))
            ->add('comments', TextareaType::class, array('label'=>'Commentaires', 'required' => false))
            ->add('hypothesis', TextareaType::class, array('label'=>'Hypotheses', 'required' => false))
            ->add('conclusions', TextareaType::class, array('label'=>'Conclusions', 'required' => false))
            ->add('experiments', TextareaType::class, array('label'=>'Experiments', 'required' => false))
            ->add('links', TextareaType::class, array('label'=>'Links', 'required' => false))
            ->add('contributors', TextareaType::class, array('label'=>'Contributors', 'required' => false))
            ->add('ajouterArticle', SubmitType::class, array('label'=>'Ajouter l\'article'))
            ->add('annuler', SubmitType::class, array('label' => 'Annuler', 'attr' => array('formnovalidate'=>'formnovalidate')))
        ;
    }

    public function getBlockPrefix()
    {
        return 'CreateArticleForm';
    }
}
?>



