<?php


namespace TPSymfony\BibliBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;


class SuppressionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
          //  ->add('idArticle', 'integer', array('label'=>'Identifiant de l\'article'))
            ->add('title', 'text', array('label'=>'Titre de l\'article'))
            ->add('date', DateType::class, array('widget'=>'choice', 'label' => 'Date de l\'article', 'format' => 'dd MM yyyy'))
            ->add('tags','text',array('label' => 'Tags pour l\'article', 'required' => false))
            ->add('brochure', 'text', array('label' => 'Brochure (PDF file)'))
            ->add('supprimerArticle', SubmitType::class, array('label'=>'Supprimer l\'article'))
            ->add('annuler', SubmitType::class, array('label' => 'Annuler'));
    }

    public function getBlockPrefix()
    {
        return 'suppressionForm';
    }

}
?>
