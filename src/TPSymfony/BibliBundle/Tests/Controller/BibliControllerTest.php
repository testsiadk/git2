<?php

namespace TPSymfony\BibliBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use TPSymfony\BibliBundle\Entity\Article as Article;

class BibliControllerTest extends WebTestCase
{

    private $application;
    private $em;

    public function __construct()
    {
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
        $this->application->setAutoExit(false);
    }

    protected function runConsole($command, Array $options = array())
    {
        $options = array_merge($options,array('command' => $command));
        return $this->application->run(new \Symfony\Component\Console\Input\ArrayInput($options));
    }

    protected function setUp ()
    {
        parent::setUp();
        $this->runConsole('doctrine:schema:drop', array( '--force' => true));
        $this->runConsole('doctrine:schema:create');
    }


    public function testLinkAjoutArticle()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ajouterArticle');
        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::ajouterAction', $client->getRequest()->attributes->get('_controller'));
        $this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function testLinkModifArticle()
    {
        $client = static::createClient();

        //$crawler = $client->request('GET', '/modifierArticle/1/fre');
        //$this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::modifierAction', $client->getRequest()->attributes->get('_controller'));
        //$this->assertTrue(200 === $client->getResponse()->getStatusCode());
    }

    public function testLinklisteDesArticlesAuteur()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/listeDesArticlesAuteur');
        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::afficherArticleAction', $client->getRequest()->attributes->get('_controller'));

    }

    public function testLinklisteDesArticles()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/listeDesArticles');
        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::afficherArticle2Action', $client->getRequest()->attributes->get('_controller'));

    }

    public function testAjouterArticleForm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ajouterArticle');
        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::ajouterAction', $client->getRequest()->attributes->get('_controller'));

        $form = $crawler->selectButton('Ajouter l\'article')->form(array(
            'CreateArticleForm[title]'      => 'TestAjoutArticle',
            'CreateArticleForm[subtitle]'   => 'Test',
            'CreateArticleForm[type]'       => 'Book',
            'CreateArticleForm[isbn]'       => '123-ABC',
            'CreateArticleForm[editor]'     => 'SIA',
            'CreateArticleForm[date]'       => array('year' => 2013, 'month' => 06, 'day' => 05),
            'CreateArticleForm[tags]'       => 'test',
            'CreateArticleForm[brochure]'   => __DIR__.'/../../../../../web/brochures/RapportSymfony.pdf',
            'CreateArticleForm[comments]'   => 'Super article',
            'CreateArticleForm[hypothesis]' => 'You will work with symfony to develop websites for our customers.',
            'CreateArticleForm[conclusions]'=> 'Send me an email',
        ));

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirection());

        $client->followRedirect();

        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::ajouterOkAction', $client->getRequest()->attributes->get('_controller'));
    }

    public function testAjouterEtAnnulerArticleForm()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ajouterArticle');
        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::ajouterAction', $client->getRequest()->attributes->get('_controller'));

        $form = $crawler->selectButton('Annuler')->form(array(
            'CreateArticleForm[title]'      => 'TestAjoutArticle',
            'CreateArticleForm[date]'       => array('year' => 2013, 'month' => 06, 'day' => 05),
            'CreateArticleForm[tags]'       => 'test',
            'CreateArticleForm[brochure]'   => __DIR__.'/../../../../../web/brochures/certifNAVIGO.pdf',
            'CreateArticleForm[comments]'   => 'Super article',
            'CreateArticleForm[hypothesis]' => 'You will work with symfony to develop websites for our customers.',
            'CreateArticleForm[conclusions]'=> 'Send me an email',
        ));

        $client->submit($form);

        $this->assertTrue($client->getResponse()->isRedirection());

        $client->followRedirect();

        $this->assertEquals('TPSymfony\BibliBundle\Controller\BibliController::afficherArticleAction', $client->getRequest()->attributes->get('_controller'));
    }

    public function testReferencementSimple(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        // $article->setDate("2018/06/01");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(0,$referencement);
    }


    public function testReferencementHypothesis(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setHypothesis("Ceci est une hypothese");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(10,$referencement);
    }

    public function testReferencementConclusion(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setConclusions("Ceci est une conclusion");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(20,$referencement);
    }

    public function testReferencementComments(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setComments("Ceci est un commentaire");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(10,$referencement);
    }

    public function testReferencementLinks(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setLinks("Ceci est un lien");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(10,$referencement);
    }

    public function testReferencementExperiments(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setExperiments("Experimentation1");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(10,$referencement);
    }

    public function testReferencementContributors(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setContributors("The gazelles");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(20,$referencement);
    }

    public function testReferencementDataSet(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setDataset("Dataset");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(20,$referencement);
    }

    public function testReferencementMixte(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setHypothesis("Ceci est une hypothese");  //10
        $article->setConclusions("Ceci est une conclusion");    //20
        $article->setLinks("Ceci est un lien");     //10
        $article->setExperiments("Experimentation1");   //10
        $article->setContributors("The gazelles");  //20
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(70,$referencement);
    }

    public function testReferencementComplet(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestReferencement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setHypothesis("Ceci est une hypothese");
        $article->setConclusions("Ceci est une conclusion");
        $article->setComments("Ceci est un commentaire");
        $article->setLinks("Ceci est un lien");
        $article->setExperiments("Experimentation1");
        $article->setContributors("The gazelles");
        $article->setDataset("Dataset");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();
        $referencement = $article->getReferencement();

        $this->assertEquals(100,$referencement);
    }


    public function testEnregistrementBDD(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestEnregistrement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $article->setHypothesis("Ceci est une hypothese");
        $article->setConclusions("Ceci est une conclusion");
        $article->setReferencement();
        $em->persist($article);
        $em->flush();

        $query = $em->createQuery("SELECT COUNT(a.idArticle) FROM TPSymfonyBibliBundle:Article a WHERE a.title='TestEnregistrement'");
        $this->assertEquals(1, $query->getSingleScalarResult());
    }

    public function testModificationArticle(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();

        $article = new Article();
        $article->setTitle("TestEnregistrement");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $this->assertEquals("test", $article->getTags());
        $article->setTags("Tag modifié");
        $this->assertEquals("Tag modifié", $article->getTags());
    }

    public function testSuppressionArticle(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestSuppression");
        $article->setTags("test");
        $article->setBrochure("RapportSymfony.pdf");
        $em->remove($article);
        $query = $em->createQuery("SELECT COUNT(a.idArticle) FROM TPSymfonyBibliBundle:Article a WHERE a.title='TestSuppression'");
        $this->assertEquals(0, $query->getSingleScalarResult());
    }

    public function testNbArticle(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestStatistics : article 1");
        $article->setTags("test");
        $article->setReferencement();
        $article->setType("InCollection");
        $em->persist($article);

        $article2 = new Article();
        $article2->setTitle("TestStatics :  article 2");
        $article2->setBrochure("RapportSymfony.pdf");
        $article2->setDate(new \DateTime());
        $article2->setReferencement();
        $article2->setType("Book");
        $em->persist($article2);

        $article3 = new Article();
        $article3->setTitle("TestStatistics : article 3");
        $article3->setTags("test");
        $article3->setReferencement();
        $article3->setType("inproceedings");
        $em->persist($article3);

        $article4 = new Article();
        $article4->setTitle("TestStatistics : article 3");
        $article4->setTags("test");
        $article4->setReferencement();
        $article4->setType("Article");
        $em->persist($article3);
        $em->flush();

        $client = static::createClient();
        $crawler = $client->request('GET', '/statistiques');
        $this->assertContains(3,$client->getResponse()->getContent("Nombre d'articles"));
    }

    public function testNbInCollection(){
        $kernel = new \AppKernel('test',true);
        $kernel->boot();
        $em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $article = new Article();
        $article->setTitle("TestStatistics : article 1");
        $article->setTags("test");
        $article->setReferencement();
        $article->setType("InCollection");
        $em->persist($article);
        $em->flush();

        $client = static::createClient();
        $crawler = $client->request('GET', '/statistiques');
        $this->assertContains("1",$client->getResponse()->getContent("Nombre de InCollection"));
    }


}
