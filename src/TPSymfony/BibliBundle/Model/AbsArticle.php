<?php

namespace TPSymfony\BibliBundle\Model;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Article
 *
 * @ORM\MappedSuperClass
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap({"article"="Article"})
 *
 */
abstract class AbsArticle{

    /**
     * var int $referencement
     * @ORM\Column(name="referencement", type="integer", nullable=true)
     */
    protected $referencement;

    /**
     * Get referencement
     *
     * @return string
     */
    public function getReferencement()
    {
        return $this->referencement;
    }



}