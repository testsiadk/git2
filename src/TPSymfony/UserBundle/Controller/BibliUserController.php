<?php

namespace TPSymfony\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\SecurityContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use TPSymfony\BibliBundle\Controller\BibliController;


class BibliUserController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('TPSymfonyBibliBundle:Default:index.html.twig');
    }




    /**
     * @Route("/listeArticleAuteur", name="admin_page")
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function loginOkAction()

    {
       return $this->render('TPSymfonyUserBundle:Default:mesArticles.html.twig');


    }




}

?>