<?php

namespace TPSymfony\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use TPSymfony\BibliBundle\Entity\Article as Article;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * User
 *
 * @ORM\Table(name="bibli_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="TPSymfony\BibliBundle\Entity\Article", mappedBy="user")
     */
    private $articles;


    public function __construct()
    {
        parent::__construct();
        $this->articles = new ArrayCollection();
        // your own logic
    }

    /**
     * Add article
     *
     * @param Article $article
     *
     * @return User
     */
    public function addArticle(Article $article)
    {
        $this->articles[] = $article;

        return $this;
    }

    /**
     * Remove article
     *
     * @param Article $article
     */
    public function removeArticle(Article $article)
    {
        $this->articles->removeElement($article);
    }

    /**
     * Get articles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticles()
    {
        return $this->articles;
    }

    /**
     * @return mixed
     */
    public function getId(){
    return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id){
    $this->id = $id;
    }

}
