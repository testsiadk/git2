<?php

namespace TPSymfony\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class TPSymfonyUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
